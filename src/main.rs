#![feature(nll)]

use fms::prelude::*;

use std::{io, thread};
use tui::Terminal;
use tui::backend::{TermionBackend, Backend};
use termion::raw::IntoRawMode;
use termion::cursor::Goto;
use termion::event::Key;
use crate::event::{Events, Event};
use termion::input::TermRead;
use std::io::{Read, Write};
use crate::ui::Ui;

mod event;
mod util;
mod config;
mod ui;

use config::*;
use std::fs::File;
use std::path::Path;
use std::time::Duration;

type Result<T> = std::result::Result<T, failure::Error>;

fn main() -> Result<()> {
    println!("Rusty FMS");
    println!("Don't be stupid stupid. Robots can hurt");

    let conf_path = Path::new("timings.json");

    if !conf_path.exists() {
        println!("timings.json does not exist. Prepopulating with default values...");
        let mut f = File::create("timings.json")?;
        serde_json::to_writer_pretty(f, &DEFAULT_TIMINGS)?;
    }

    let conf = File::open("timings.json")?;
    let timings: MatchTimings = serde_json::from_reader(conf)?;

    println!("Using match configuration: {:#?}", timings);
    println!("To adjust, change values in timings.json");

    let mut contexts = [
        FMSContext::new_invalid(1, AllianceStation::Red(1)),
        FMSContext::new_invalid(1, AllianceStation::Red(1)),
        FMSContext::new_invalid(1, AllianceStation::Red(1)),
        FMSContext::new_invalid(1, AllianceStation::Red(1)),
        FMSContext::new_invalid(1, AllianceStation::Red(1)),
        FMSContext::new_invalid(1, AllianceStation::Red(1)),
    ];

    println!();
    let stdin = io::stdin();
    let mut stdout = io::stdout();

    for i in 1u16..=3 {
        print!("Enter team number for Red {}: ", i);
        stdout.flush()?;

        let team_number = stdin.lock().read_line()?;
        util::fill_context(&mut contexts, (i - 1) as usize, team_number, AllianceStation::Red(i as u8));
    }

    for i in 1u16..=3 {
        print!("Enter team number for Blue {}: ", i);
        stdout.flush()?;
        let team_number = stdin.lock().read_line()?;
        util::fill_context(&mut contexts, (i + 2) as usize, team_number, AllianceStation::Blue(i as u8));
    }

    let mut fms = FieldManagementSystem::new()?;

    let m = Match::new(contexts,
                       TournamentLevel::Test,
                       1,
                       timings.autonomous_duration,
                       timings.timeout_duration,
                       timings.teleop_duration,
                       timings.start_of_endgame);
    fms.load_match(m);

    let stdout = stdout.into_raw_mode()?;
    let mut ui = Ui::new(stdout)?;

    ui.run(fms).unwrap();
    Ok(())
}
