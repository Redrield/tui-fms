use tui::Terminal;
use tui::backend::TermionBackend;
use termion::raw::RawTerminal;
use std::io::Stdout;
use crate::Result;
use crate::event::{Events, Event};
use fms::prelude::*;
use tui::layout::*;
use tui::widgets::*;
use tui::style::*;
use termion::event::Key;
use chrono::Utc;

mod views;

pub struct Ui {
    term: Terminal<TermionBackend<RawTerminal<Stdout>>>,
}

impl Ui {
    pub fn new(stdout: RawTerminal<Stdout>) -> Result<Ui> {
        let mut term = Terminal::new(TermionBackend::new(stdout))?;
        term.clear()?;
        term.hide_cursor()?;

        Ok(Ui {
            term,
        })
    }

    pub fn run(&mut self, fms: FieldManagementSystem) -> Result<()> {
        let mut events = Events::new();

        let mut ready = false;
        let mut started = false;

        let mut last_time = Utc::now().timestamp_millis();

        loop {
            self.term.draw(|mut f| {
                let top_level = Layout::default()
                    .constraints([Constraint::Percentage(40), Constraint::Percentage(60)].as_ref())
                    .direction(Direction::Vertical)
                    .split(f.size());

                let alliance_portions = Layout::default()
                    .constraints([
                        Constraint::Percentage(50),
                        Constraint::Percentage(50),
                    ].as_ref())
                    .direction(Direction::Horizontal)
                    .split(top_level[0]);

                alliance_portions.iter().enumerate().for_each(|(i, rect)| {
                    Block::default().borders(Borders::ALL)
                        .title(if i == 0 { "Red Alliance" } else { "Blue Alliance" })
                        .title_style(Style::default().fg(if i == 0 { Color::Red } else { Color::Blue }))
                        .render(&mut f, *rect)
                });

                let team_indicators = alliance_portions.into_iter().flat_map(|rect| Layout::default()
                    .constraints([Constraint::Percentage(33), Constraint::Percentage(34), Constraint::Percentage(33)].as_ref())
                    .margin(1)
                    .direction(Direction::Horizontal)
                    .split(rect)).collect::<Vec<Rect>>();

                if !started {
                    views::draw_ds_prematch(&mut f, team_indicators, &fms);
                } else {
                    views::draw_ds_match(&mut f, team_indicators, &fms);
                }

                let lower_screen = Layout::default().constraints([Constraint::Percentage(10), Constraint::Percentage(80), Constraint::Percentage(10)].as_ref())
                    .direction(Direction::Horizontal)
                    .split(top_level[1]);

                if fms.match_ready() && !fms.started() && fms.current_mode() != Some(Mode::Transition) {
                    if !fms.aborted() {
                        Paragraph::new([
                            Text::styled("Ready to Start\n\n", Style::default().fg(Color::Green).modifier(Modifier::SLOW_BLINK | Modifier::BOLD)),
                            Text::raw("Press Enter to start")
                        ].iter())
                            .block(Block::default().borders(Borders::ALL).title("Prematch Status"))
                            .render(&mut f, lower_screen[1]);
                        ready = true;
                    } else {
                        Paragraph::new([
                            Text::styled("Match Aborted", Style::default().fg(Color::Red).modifier(Modifier::BOLD | Modifier::SLOW_BLINK))
                        ].iter())
                            .block(Block::default().borders(Borders::ALL).title("Match Status"))
                            .render(&mut f, lower_screen[1]);
                    }
                } else {
                    if fms.started() || fms.current_mode() == Some(Mode::Transition) {
                        Paragraph::new([
                            Text::styled("Match Running\n", Style::default().fg(Color::Green)),
                            Text::raw(&format!("Current mode: {:?}\n", fms.current_mode().unwrap())),
                            Text::raw(&format!("Remaining time: {}", fms.remaining_time()))
                        ].iter())
                            .block(Block::default().borders(Borders::ALL).title("Match Status"))
                            .render(&mut f, lower_screen[1]);
                    } else if fms.aborted() {
                        Paragraph::new([
                            Text::styled("Match Aborted", Style::default().fg(Color::Red).modifier(Modifier::BOLD | Modifier::SLOW_BLINK))
                        ].iter())
                            .block(Block::default().borders(Borders::ALL).title("Match Status"))
                            .render(&mut f, lower_screen[1]);
                    } else {
                        ready = false;
                        if started {
                            Paragraph::new([
                                Text::raw("Match cycle completed.\n"),
                                Text::raw("Press 'q' to quit.")
                            ].iter())
                                .block(Block::default().borders(Borders::ALL).title("Post-Match"))
                                .render(&mut f, lower_screen[1])
                        } else {
                            Paragraph::new([
                                Text::styled("Not ready", Style::default().fg(Color::Red))
                            ].iter())
                                .block(Block::default().borders(Borders::ALL).title("Prematch Status"))
                                .render(&mut f, lower_screen[1])
                        }
                    }
                }
            })?;

            match events.next()? {
                Event::Input(input) => match input {
                    Key::Char('q') => return Ok(()),
                    Key::Char('\n') => if ready && !started {
                        started = true;
                        fms.start();
                    }
                    // Estop everyone
                    Key::Char(' ') => if fms.started() {
                        fms.estop();
                    }
                    // Estop specific player station
                    Key::Char('1') => if fms.started() {
                        fms.estop_team(fms.contexts()[0].team_number);
                    }
                    Key::Char('2') => if fms.started() {
                        fms.estop_team(fms.contexts()[1].team_number);
                    }
                    Key::Char('3') => if fms.started() {
                        fms.estop_team(fms.contexts()[2].team_number);
                    }
                    Key::Char('4') => if fms.started() {
                        fms.estop_team(fms.contexts()[3].team_number);
                    }
                    Key::Char('5') => if fms.started() {
                        fms.estop_team(fms.contexts()[4].team_number);
                    }
                    Key::Char('6') => if fms.started() {
                        fms.estop_team(fms.contexts()[5].team_number);
                    }
                    _ => {}
                }
                Event::Tick => {}
            }
        }
    }
}

impl Drop for Ui {
    fn drop(&mut self) {
        self.term.clear().unwrap();
        self.term.show_cursor().unwrap();
    }
}
