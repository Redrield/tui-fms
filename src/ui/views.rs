use tui::backend::Backend;
use tui::Frame;
use fms::prelude::*;
use tui::layout::*;
use tui::widgets::*;
use tui::style::*;

pub fn draw_ds_prematch<B: Backend>(f: &mut Frame<B>, team_views: Vec<Rect>, fms: &FieldManagementSystem) {
    let mut connected_stations = fms.connected_stations();

    let stations = connected_stations.clone();
    let disconnected = fms.contexts().into_iter().filter(|ctx| !stations.iter().any(|station| station.ctx == *ctx))
        .map(|ctx| PlayerStationContext {
            ctx: FMSContext::new_invalid(ctx.team_number, ctx.alliance_station),
            status: Status::empty(),
        });

    connected_stations.extend(disconnected); // Add some entries for disconnected DSes. (i think) this is the only time we'll get Status::empty()

    for (i, station) in connected_stations.iter().enumerate() {
        //TODO: Find a better way for conditional text like this
        let ready = station.status.contains(Status::RADIO_COMMS_ACTIVE | Status::RIO_PING);
        let ready_text = if ready {
            Text::styled("Ready\n", Style::default().fg(Color::Green).modifier(Modifier::BOLD))
        } else {
            Text::styled("Not Ready\n", Style::default().fg(Color::Red).modifier(Modifier::BOLD))
        };

        let ds_text = if station.ctx.is_valid() {
            Text::styled("true\n", Style::default().fg(Color::Green))
        } else {
            Text::styled("false\n", Style::default().fg(Color::Red))
        };

        let radio_text = if station.status.contains(Status::RADIO_PING) || station.status.contains(Status::RADIO_COMMS_ACTIVE) {
            Text::styled("true\n", Style::default().fg(Color::Green))
        } else {
            Text::styled("false\n", Style::default().fg(Color::Red))
        };

        let roborio_text = if station.status.contains(Status::RIO_PING) {
            Text::styled("true", Style::default().fg(Color::Green))
        } else {
            Text::styled("false", Style::default().fg(Color::Red))
        };

        Paragraph::new([
            ready_text,
            Text::raw("DS Connected: "),
            ds_text,
            Text::raw("Radio connected: "),
            radio_text,
            Text::raw("roboRIO Connected: "),
            roborio_text
        ].iter())
            .block(Block::default()
                .title(&format!("{}", station.ctx.team_number))
                .borders(Borders::ALL))
            .render(f, team_views[station.ctx.alliance_station.as_u8() as usize])
    }
}

pub fn draw_ds_match<B: Backend>(f: &mut Frame<B>, team_views: Vec<Rect>, fms: &FieldManagementSystem) {
    let mut connected_stations = fms.connected_stations();

    let stations = connected_stations.clone();
    let disconnected = fms.contexts().into_iter().filter(|ctx| !stations.iter().any(|station| station.ctx == *ctx))
        .map(|ctx| PlayerStationContext {
            ctx: FMSContext::new_invalid(ctx.team_number, ctx.alliance_station),
            status: Status::empty(),
        });

    connected_stations.extend(disconnected); // Add some entries for disconnected DSes. (i think) this is the only time we'll get Status::empty()

    for (i, station) in connected_stations.iter().enumerate() {
        //TODO: Find a better way for conditional text like this
        let enabled = station.status.contains(Status::ENABLED);
        let status_text = if enabled {
            Text::styled("Enabled\n", Style::default().fg(Color::Green).modifier(Modifier::BOLD))
        } else if station.status.contains(Status::ESTOP) {
            Text::styled("Emergency Stopped\n", Style::default().fg(Color::Red).modifier(Modifier::BOLD | Modifier::SLOW_BLINK))
        } else {
            Text::styled("Disabled\n", Style::default().fg(Color::Red).modifier(Modifier::BOLD))
        };

        let ds_text = if !station.status.is_empty() {
            Text::styled("true\n", Style::default().fg(Color::Green))
        } else {
            Text::styled("false\n", Style::default().fg(Color::Red))
        };

        let radio_text = if station.status.contains(Status::RADIO_PING) || station.status.contains(Status::RADIO_COMMS_ACTIVE) {
            Text::styled("true\n", Style::default().fg(Color::Green))
        } else {
            Text::styled("false\n", Style::default().fg(Color::Red))
        };

        let roborio_text = if station.status.contains(Status::RIO_PING) {
            Text::styled("true", Style::default().fg(Color::Green))
        } else {
            Text::styled("false", Style::default().fg(Color::Red))
        };

        Paragraph::new([
            status_text,
            Text::raw("DS Connected: "),
            ds_text,
            Text::raw("Radio connected: "),
            radio_text,
            Text::raw("roboRIO Connected: "),
            roborio_text
        ].iter())
            .block(Block::default()
                .title(&format!("{}", station.ctx.team_number))
                .borders(Borders::ALL))
            .render(f, team_views[station.ctx.alliance_station.as_u8() as usize])
    }
}