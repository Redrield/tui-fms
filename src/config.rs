use serde::{Serialize, Deserialize};
use serde_derive::*;

pub const DEFAULT_TIMINGS: MatchTimings = MatchTimings {
    autonomous_duration: 15f64,
    timeout_duration: 1f64,
    teleop_duration: 135f64,
    start_of_endgame: 30f64
};


#[derive(Serialize, Deserialize, Debug)]
pub struct MatchTimings {
    pub autonomous_duration: f64,
    pub timeout_duration: f64,
    pub teleop_duration: f64,
    pub start_of_endgame: f64
}
