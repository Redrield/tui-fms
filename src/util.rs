use fms::prelude::*;

pub fn fill_context(arr: &mut [FMSContext; 6], idx: usize, entry: Option<String>, station: AllianceStation) {
    arr[idx] = match entry.unwrap().parse::<u16>() {
        Ok(tn) => FMSContext::new(tn, station),
        Err(_) => FMSContext::new_invalid(idx as u16 + 1, station)
    }
}